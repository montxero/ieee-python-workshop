# Setup
The following steps creates an environment suitable for following this
workshop.

2. In a terminal, navigate to `ieee-python-workshop`.
3. Create a **venv** virtual environment in the current directory with
   `$ python -m venv .venv`
4. Activate the virtual environment.
   - If in powershell: `$ .\.venv\Scripts\activate`
   - In a unix-like os, `$ . .venv/bin/acitvate`
5. Install Jupyterlab, numpy and matplotlib with
`(.venv)$ pip install jupyterlab numpy matplotlib`

## Post Setup
You can launch a new Jupyterlab session with `(.venv) $ jupyter-lab`
